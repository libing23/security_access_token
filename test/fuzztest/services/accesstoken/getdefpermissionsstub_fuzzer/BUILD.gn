# Copyright (c) 2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/config/features.gni")
import("//build/test.gni")
import("../../../../../access_token.gni")

ohos_fuzztest("GetDefPermissionsStubFuzzTest") {
  module_out_path = module_output_path_service_access_token
  fuzz_config_file = "."

  sources = [ "getdefpermissionsstub_fuzzer.cpp" ]

  cflags = [
    "-g",
    "-O0",
    "-Wno-unused-variable",
    "-fno-omit-frame-pointer",
  ]

  include_dirs = [
    "${access_token_path}/frameworks/accesstoken/include",
    "${access_token_path}/services/accesstokenmanager/main/cpp/include",
  ]

  deps = [
    "${access_token_path}/interfaces/innerkits/accesstoken:libaccesstoken_sdk",
    "${access_token_path}/interfaces/innerkits/token_setproc:libperm_setproc",
    "${access_token_path}/interfaces/innerkits/token_setproc:libtoken_setproc",
  ]

  configs = [ "${access_token_path}/config:coverage_flags" ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
    "ipc:ipc_core",
    "safwk:system_ability_fwk",
  ]

  include_dirs += [
    "${access_token_path}/services/accesstokenmanager/main/cpp/include/callback",
    "${access_token_path}/services/accesstokenmanager/main/cpp/include/service",
    "${access_token_path}/services/accesstokenmanager/main/cpp/include/token",
    "${access_token_path}/services/accesstokenmanager/main/cpp/include/permission",
    "${access_token_path}/services/accesstokenmanager/main/cpp/include/database",
    "${access_token_path}/frameworks/common/include",
    "${access_token_path}/frameworks/accesstoken/include",
    "${access_token_path}/interfaces/innerkits/privacy/include",
    "${access_token_path}/interfaces/innerkits/tokensync/src",
    "${access_token_path}/services/common/app_manager/include",
    "${access_token_path}/services/common/database/include",
    "${access_token_path}/services/common/handler/include",
    "//third_party/sqlite/include/",
    "//third_party/json/include/",
  ]

  include_dirs += access_token_impl_include_dirs

  cflags_cc = [ "-DHILOG_ENABLE" ]

  sources += [
    "${access_token_path}/services/accesstokenmanager/main/cpp/src/callback/callback_manager.cpp",
    "${access_token_path}/services/accesstokenmanager/main/cpp/src/callback/perm_state_callback_death_recipient.cpp",
    "${access_token_path}/services/accesstokenmanager/main/cpp/src/callback/permission_state_change_callback_proxy.cpp",
    "${access_token_path}/services/accesstokenmanager/main/cpp/src/database/access_token_db.cpp",
    "${access_token_path}/services/accesstokenmanager/main/cpp/src/database/data_translator.cpp",
    "${access_token_path}/services/accesstokenmanager/main/cpp/src/database/token_field_const.cpp",
    "${access_token_path}/services/accesstokenmanager/main/cpp/src/permission/permission_definition_cache.cpp",
    "${access_token_path}/services/accesstokenmanager/main/cpp/src/permission/permission_grant_event.cpp",
    "${access_token_path}/services/accesstokenmanager/main/cpp/src/permission/permission_manager.cpp",
    "${access_token_path}/services/accesstokenmanager/main/cpp/src/permission/permission_policy_set.cpp",
    "${access_token_path}/services/accesstokenmanager/main/cpp/src/permission/permission_validator.cpp",
    "${access_token_path}/services/accesstokenmanager/main/cpp/src/permission/system_permission_definition_parser.cpp",
    "${access_token_path}/services/accesstokenmanager/main/cpp/src/permission/temp_permission_observer.cpp",
    "${access_token_path}/services/accesstokenmanager/main/cpp/src/service/accesstoken_manager_service.cpp",
    "${access_token_path}/services/accesstokenmanager/main/cpp/src/service/accesstoken_manager_stub.cpp",
    "${access_token_path}/services/accesstokenmanager/main/cpp/src/token/accesstoken_id_manager.cpp",
    "${access_token_path}/services/accesstokenmanager/main/cpp/src/token/accesstoken_info_manager.cpp",
    "${access_token_path}/services/accesstokenmanager/main/cpp/src/token/hap_token_info_inner.cpp",
    "${access_token_path}/services/accesstokenmanager/main/cpp/src/token/native_token_info_inner.cpp",
    "${access_token_path}/services/accesstokenmanager/main/cpp/src/token/native_token_receptor.cpp",
  ]

  sources += access_token_impl_sources

  if (dlp_permission_enable == true) {
    cflags_cc += [ "-DSUPPORT_SANDBOX_APP" ]
    sources += [
      "${access_token_path}/services/accesstokenmanager/main/cpp/src/permission/dlp_permission_set_manager.cpp",
      "${access_token_path}/services/accesstokenmanager/main/cpp/src/permission/dlp_permission_set_parser.cpp",
    ]
  }

  deps += [
    "${access_token_path}/frameworks/accesstoken:accesstoken_communication_adapter_cxx",
    "${access_token_path}/frameworks/common:accesstoken_common_cxx",
    "${access_token_path}/interfaces/innerkits/accesstoken:libtokenid_sdk",
    "${access_token_path}/interfaces/innerkits/privacy:libprivacy_sdk",
    "${access_token_path}/services/accesstokenmanager:access_token.rc",
    "${access_token_path}/services/common:accesstoken_service_common",
  ]

  deps += [ "${access_token_path}/services/accesstokenmanager/etc:param_files" ]

  external_deps += [
    "c_utils:utils",
    "dsoftbus:softbus_client",
    "hilog:libhilog",
    "hisysevent:libhisysevent",
    "hitrace:hitrace_meter",
    "init:libbegetutil",
    "ipc:ipc_core",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]

  if (eventhandler_enable == true) {
    cflags_cc += [ "-DEVENTHANDLER_ENABLE" ]
    external_deps += [ "eventhandler:libeventhandler" ]
  }

  if (hicollie_enable == true) {
    external_deps += [ "hicollie:libhicollie" ]
    cflags_cc += [ "-DHICOLLIE_ENABLE" ]
  }

  if (token_sync_enable == true) {
    cflags_cc += [ "-DTOKEN_SYNC_ENABLE" ]

    sources += [
      "${access_token_path}/services/accesstokenmanager/main/cpp/src/token/accesstoken_remote_token_manager.cpp",
      "${access_token_path}/services/accesstokenmanager/main/cpp/src/token/token_modify_notifier.cpp",
    ]

    include_dirs +=
        [ "${access_token_path}/interfaces/innerkits/tokensync/include" ]

    deps += [
      "${access_token_path}/interfaces/innerkits/tokensync:libtokensync_sdk",
    ]
  }
}
